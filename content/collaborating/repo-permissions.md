---
eleventyNavigation:
  key: RepositoryPermissions
  title: Repository Permissions
  parent: Collaborating
  order: 50
---

When you invite collaborators to join your repository (see [Invite Collaborators](/collaborating/invite-collaborators)) or when you create teams for your organization (see [Create and Manage an Organization](/collaborating/create-organization)), you have to decide what each collaborator/team is allowed to do.

There are four permission levels: Read, Write, Administrator and Owner.  
The owner is the one who created the repository, or, in case of an organization, all members of the team "Owners" (which by default includes the creator of the organization).

The table below gives an overview of what collaborators/teams are allowed to do when granted each of these permission levels:

<table class="table">
 <thead>
  <tr>
   <th> Task </th>
   <th> Read </th>
   <th> Write</th>
   <th> Admin </th>
   <th> Owner </th>
 </thead>
 <tbody>
  <tr>
   <td> View, clone and pull repository </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Contribute pull requests </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Push to/update contributed pull requests </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Push directly to repository </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Merge pull requests </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Moderate/delete issues and comments </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Force-push/rewrite history (if enabled) </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Add/remove collaborators to repository </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Configure branch settings (protect/unprotect, enable force-push) </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  <tr>
   <td> Configure repository settings (enable wiki, issues, PRs, update profile) </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  <tr>
   <td> Configure repository settings in the danger zone (transfer ownership, delete wiki data / repository, archive repository) </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-times" style="color: red"></i> </td>
   <td> <i class="fas fa-check" style="color: green"></i> </td>
  </tr>
  </tr>
 </body>
</table>
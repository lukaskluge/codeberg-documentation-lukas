---
eleventyNavigation:
  key: UsingTags
  title: Tags and Releases
  parent: Git
  order: 60
---

## What are tags?
Tags are a Git function used to make a snapshot of a repository. It is generally used to mark releases (e.g. v1.2.4), and functions like a shortcut to what the repo looked like at the time.

## What are releases?
Releases are a feature independent of Git that allows you to attach files and release notes along and share it on Codeberg, linking to a Git tag.

### Wait, what is the difference between tags and releases?
They are very similar, the difference being that tags are just the repository frozen in time and are part of Git (You can make one using Git), releases are tags accompanied with a binary file and are not part of Git (You need to go to your Codeberg repository page to add the binary).

## Creating tags and releases
If you want to create tags it is recommended to do it using Git. You can also create tags during the release creation process on Codeberg. Releases can only be created from Codeberg.

> Tags are generally labelled by version numbers. It is good practice to prefix a version number with a `v` (e.g. `v1.2.3`) and to use the [Semantic Versioning](https://semver.org/) specification for assigning and incrementing version numbers. 

### On Git
To create a tag using git, use the following command in your repo folder.
```bash
git tag -a <version number here> -m "<my tag name>"
```

### On Codeberg
To create a release on Codeberg, first go to the `Releases` tab of your repository, and click on `New Release`:

<picture>
  <source srcset="/assets/images/collaborating/citable-code/releases1.webp" type="image/webp">
  <img src="/assets/images/collaborating/citable-code/releases1.png" alt="releases1">
</picture>

In the new page, enter a version number for your new release, select the branch that contains the code you want to release, add a title and a description:

<picture>
  <source srcset="/assets/images/collaborating/citable-code/new-release.webp" type="image/webp">
  <img src="/assets/images/collaborating/citable-code/new-release.png" alt="new-release">
</picture>

You can either save as a draft, or publish the release outright.  

You are then re-directed to the `Releases` tab of your repository. The newly created release is now listed there:

<picture>
  <source srcset="/assets/images/collaborating/citable-code/releases2.webp" type="image/webp">
  <img src="/assets/images/collaborating/citable-code/releases2.png" alt="releases2">
</picture>

Here you can edit the release if needed, and you can also download the source code in ZIP or TAR.GZ format.

## Finding and viewing releases in a repository
<picture>
  <source srcset="/assets/images/git/using-tags/tags.webp" type="image/webp">
  <img src="/assets/images/git/using-tags/tags.png" alt="tags">
</picture>

To view the release, go to the releases tab (1) in the repository. Then locate the release you want to view. As an example, we will be looking at the `Added Knut!` release (2). If you just want to access the files attached to the release, you can download it from the `Downloads` dropdown (3). 
If you want to see a snapshot of the source code at the time of the release, select a source code archive download (4) from the `Downloads` dropdown or click on the tag on the left side (5).
